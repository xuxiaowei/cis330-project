~~~~REPORT FOR BEIJINGDUCKS~~~~

Contributions by each team member:
	Xu Xiaowei coded an AI program for human-vs-computer Gomoku.
	Lei Li and Chen Ling coded a GUI and interactive user program using the Angel2D game engine.
	Matthew Haynes coded a client and server for human-vs-human Gomoku from remote locations.

How Final Project differs from initial plans:
	The AI program is simpler than initially planned due to the time and space complexity of a smarter AI program.
	We were unable to consolidate the human-vs-human portion of the interactive GUI and the client-server program
in time for the presentation. There are two versions of the client-server program; the new one transmits just two integers
representing the coordinates of a newly placed game piece on the Gomoku board. The older version transmits the current
state of the entire board in a 2D integer array. Both can be found in the /cli-ser-for-game subdirectory, 
along with the test programs for each.	
	
