/*Algor.cpp
* human vs human algorithm or basic algorithm to determine win.
* Create in 2/28/2014
* Author: ChenLing , weigo Xu.
* Modify records: 3/4/2014 by ChenLing: modify it and adding head file.
*
*/
#include "Algor.h"
#include <vector>
#include <algorithm>

using namespace std;
Algor::Algor(){
	reset();
}

std::vector<std::pair<std::pair<int, int> ,long> > Algor::AiGo()
{
	
	char BoardData[2][15][15][8][2]={0};    // the matrix data
	int num,i,j,around,score=0,a=1,diff=0;
	int a1[15][15]={0},a2[15][15]={0};
	
	// fill in the matrix.
	for(num=0;num<2;num++)
		for(i=0;i<15;i++)
			for(j=0;j<15;j++)
			{
				if(matrix[0][0]==0)
				{
					for(around=0;around<8;around++)
					{
						if(num==0) diff=1;
						else diff=2;
						if(around==0&&j>=0)
						{
							for(;j-a>=0;)
							{
								if(matrix[i][j-a]==diff){score++;a++;continue;}
								else break;
							}
							BoardData[num][i][j][around][0]=score;score=0;
							if(matrix[i][j-a]==0&&j-a>=0){BoardData[num][i][j][around][1]=1;a=1;}
							else {BoardData[num][i][j][around][1]=0;a=1;}
						}
						if(around==1&&i>=0&&j>=0)
						{
							for(;i-a>=0&&j-a>=0;)
							{
								if(matrix[i-a][j-a]==diff){score++;a++;continue;}
								else break;
							}
							BoardData[num][i][j][around][0]=score;score=0;
							if(matrix[i-a][j-a]==0&&j-a>=0&&i-a>=0){BoardData[num][i][j][around][1]=1;a=1;}
							else {BoardData[num][i][j][around][1]=0;a=1;}
						}
						if(around==2&&i>=0)
						{
							for(;i-a>=0;)
							{
								if(matrix[i-a][j]==diff){score++;a++;continue;}
								else break;
							}
							BoardData[num][i][j][around][0]=score;score=0;
							if(matrix[i-a][j]==0&&i-a>=0){BoardData[num][i][j][around][1]=1;a=1;}
							else {BoardData[num][i][j][around][1]=0;a=1;}
						}
						if(around==3&&i>=0&&j<15)
						{
							for(;i-a>=0&&j+a<15;)
							{
								if(matrix[i-a][j+a]==diff){score++;a++;continue;}
								else break;
							}
							BoardData[num][i][j][around][0]=score;score=0;
							if(matrix[i-a][j+a]==0&&i-a>=0&&j+a<15){BoardData[num][i][j][around][1]=1;a=1;}
							else {BoardData[num][i][j][around][1]=0;a=1;}
						}
						if(around==4&&j<15)
						{
							for(;j+a<15;)
							{
								if(matrix[i][j+a]==diff){score++;a++;continue;}
								else break;
							}
							BoardData[num][i][j][around][0]=score;score=0;
							if(matrix[i][j+a]==0&&j+a<15){BoardData[num][i][j][around][1]=1;a=1;}
							else {BoardData[num][i][j][around][1]=0;a=1;}
						}
						if(around==5&&i<15&&j<15)
						{
							for(;i+a<15&&j+a<15;)
							{
								if(matrix[i+a][j+a]==diff){score++;a++;continue;}
								else break;
							}
							BoardData[num][i][j][around][0]=score;score=0;
							if(matrix[i+a][j+a]==0&&i+a<15&&j+a<15){BoardData[num][i][j][around][1]=1;a=1;}
							else {BoardData[num][i][j][around][1]=0;a=1;}
						}
						if(around==6&&i<15)
						{
							for(;i+a<15;)
							{
								if(matrix[i+a][j]==diff){score++;a++;continue;}
								else break;
							}
							BoardData[num][i][j][around][0]=score;score=0;
							if(matrix[i+a][j]==0&&i+a<15){BoardData[num][i][j][around][1]=1;a=1;}
							else {BoardData[num][i][j][around][1]=0;a=1;}
						}
						if(around==7&&j>=0&&i<15)
						{
							for(;i+a<15&&j-a>=0;)
							{
								if(matrix[i+a][j-a]==diff){score++;a++;continue;}
								else break;
							}
							BoardData[num][i][j][around][0]=score;score=0;
							if(matrix[i+a][j-a]==0&&i+a<15&&j-a>=0){BoardData[num][i][j][around][1]=1;a=1;}
							else {BoardData[num][i][j][around][1]=0;a=1;}
						}
					}
				}
				
			}
	
	
	//evaluate every spot.
	for(num=0;num<2;num++)
		for(i=0;i<15;i++)
			for(j=0;j<15;j++)
			{
				if(num==0) // evaluate white.
				{
					for(around=0;around<4;around++)
					{
						if((BoardData[num][i][j][around][0]+BoardData[num][i][j][around+4][0])==4
						   &&BoardData[num][i][j][around][1]==1&&BoardData[num][i][j][around+4][1]==1)
							score+=7000;
						if((BoardData[num][i][j][around][0]+BoardData[num][i][j][around+4][0])==3
						   &&BoardData[num][i][j][around][1]==1&&BoardData[num][i][j][around+4][1]==1)
							score+=301;
						if((BoardData[num][i][j][around][0]+BoardData[num][i][j][around+4][0])==2
						   &&BoardData[num][i][j][around][1]==1&&BoardData[num][i][j][around+4][1]==1)
							score+=43;
						if((BoardData[num][i][j][around][0]+BoardData[num][i][j][around+4][0])==1
						   &&BoardData[num][i][j][around][1]==1&&BoardData[num][i][j][around+4][1]==1)
							score+=11;
						if((BoardData[num][i][j][around][0]+BoardData[num][i][j][around+4][0])==4
						   &&((BoardData[num][i][j][around+4][1]==0)
							  ||(BoardData[num][i][j][around][1]==0)))
							score+=7000;
						if((BoardData[num][i][j][around][0]+BoardData[num][i][j][around+4][0])==3
						   &&((BoardData[num][i][j][around][1]==1&&BoardData[num][i][j][around+4][1]==0)
							  ||(BoardData[num][i][j][around][1]==0&&BoardData[num][i][j][around+4][1]==1)))
							score+=63;
						if((BoardData[num][i][j][around][0]+BoardData[num][i][j][around+4][0])==2
						   &&((BoardData[num][i][j][around][1]==1&&BoardData[num][i][j][around+4][1]==0)
							  ||(BoardData[num][i][j][around][1]==0&&BoardData[num][i][j][around+4][1]==1)))
							score+=6;
						if((BoardData[num][i][j][around][0]+BoardData[num][i][j][around+4][0])==1
						   &&((BoardData[num][i][j][around][1]==1&&BoardData[num][i][j][around+4][1]==0)
							  ||(BoardData[num][i][j][around][1]==0&&BoardData[num][i][j][around+4][1]==1)))
							score+=1;
						
					}
					if(score==126||score==189||score==252) score=1500;
					if(score==106) score=1000;
					
					a1[i][j]=score;score=0;
				}
				if(num==1) //evaluate black
				{
					for(around=0;around<4;around++)
					{
						
						if((BoardData[num][i][j][around][0]+BoardData[num][i][j][around+4][0])==4
						   &&BoardData[num][i][j][around][1]==1&&BoardData[num][i][j][around+4][1]==1)
							score+=30000;
						if((BoardData[num][i][j][around][0]+BoardData[num][i][j][around+4][0])==3
						   &&BoardData[num][i][j][around][1]==1&&BoardData[num][i][j][around+4][1]==1)
							score+=1500;
						if((BoardData[num][i][j][around][0]+BoardData[num][i][j][around+4][0])==2
						   &&BoardData[num][i][j][around][1]==1&&BoardData[num][i][j][around+4][1]==1)
							score+=51;
						if((BoardData[num][i][j][around][0]+BoardData[num][i][j][around+4][0])==1
						   &&BoardData[num][i][j][around][1]==1&&BoardData[num][i][j][around+4][1]==1)
							score+=16;
						if((BoardData[num][i][j][around][0]+BoardData[num][i][j][around+4][0])==4
						   &&((BoardData[num][i][j][around+4][1]==0)
							  ||(BoardData[num][i][j][around][1]==0)))
							score+=30000;
						if((BoardData[num][i][j][around][0]+BoardData[num][i][j][around+4][0])==3
						   &&((BoardData[num][i][j][around][1]==1&&BoardData[num][i][j][around+4][1]==0)
							  ||(BoardData[num][i][j][around][1]==0&&BoardData[num][i][j][around+4][1]==1)))
							score+=71;
						if((BoardData[num][i][j][around][0]+BoardData[num][i][j][around+4][0])==2
						   &&((BoardData[num][i][j][around][1]==1&&BoardData[num][i][j][around+4][1]==0)
							  ||(BoardData[num][i][j][around][1]==0&&BoardData[num][i][j][around+4][1]==1)))
							score+=7;
						if((BoardData[num][i][j][around][0]+BoardData[num][i][j][around+4][0])==1
						   &&((BoardData[num][i][j][around][1]==1&&BoardData[num][i][j][around+4][1]==0)
							  ||(BoardData[num][i][j][around][1]==0&&BoardData[num][i][j][around+4][1]==1)))
							score+=2;
					}
					if(score==142||score==213||score==284) score=1500;
					if(score==122) score=1300;
					
					a2[i][j]=score;score=0;
				}
			}

	
		
	// pick the spot with highest score
	std::vector<std::pair<std::pair<int, int> ,long> > vec;
	
	for(int i=0;i<15;i++)
		for(int j=0;j<15;j++)
		{
			if(a1[i][j]<a2[i][j])
				a1[i][j]=a2[i][j];
			vec.push_back (make_pair( make_pair(i, j), a1[i][j]));
				
		}
		
	struct sort_pred {
		bool operator()(const std::pair<pair<int, int>,long> &left, const std::pair<pair<int, int>,long> &right) {
			return left.second > right.second;
		}
	};
	
	sort(vec.begin(), vec.end(), sort_pred());
	
	return vec;
}

//***********************************************//

void Algor::reset(){
    for(int i=0;i<15;++i){
        for(int j=0;j<15;++j){
            matrix[i][j]=0;
        }}

}
void Algor::SetPos(int x, int y, int col){
        int x1,y1;
        x1 = x+7;
        y1 = 7-y;

        if(col==2){matrix[x1][y1]=2;}  //black
        else if(col ==1){matrix[x1][y1]=1;} //white

}

bool Algor::GetPos(int x,int y){

        if(x>7||x<-7){return false;}
        else if(y>7||y<-7){return false;}
        else if(matrix[x+7][7-y]==2){return false;}  //black
        else if(matrix[x+7][7-y]==1){return false;}  //white
        return true;
    }

bool Algor::isWin(int x1,int y1){
         /* human vs human algorithm. Checking for which side get five continous
         * same color, the basic idea is locate current position x1,y1 and
         *transform into the matrix position x=x1+7, y=7-y1.Then set this position
         *as center point search for up down left right bevels,and find if it have
         *five same color position. ------ChenLing
         */
         int x,y;
         x=x1+7;
         y=7-y1;
         color = matrix[x][y];
         // first calacute row number of this position,
         int counter=1;
         for(int i=1;i<5;++i){
             if(y+i>15){break;}
             else if(matrix[x][y+i]==color){
                 counter++;
                 if(counter==5){return true;}}
             else {break;}
            }

         for(int i=1;i<5;++i){
             if(y-i<0){break;}
             else if(matrix[x][y-i]==color){counter++;if(counter==5){return true;}}
             else{break;}
         }

         //if no row can have five same color, reset counter and find col.
         counter=1;
         for(int i=1;i<5;++i){
             if(x+i>15){break;}
             else if(matrix[x+i][y]==color){counter++;if(counter==5){return true;}}
             else {break;}
            }
         for(int i=1;i<5;++i){
             if(x-i<0){break;}
             else if(matrix[x-i][y]==color){counter++;if(counter==5){return true;}}
             else{break;}
         }
         // if no col have five same color, reset counter and find right bevel
         counter =1;
         for(int i=1;i<5;++i){
             if(y+i>15||x-i<0){break;}
             else if(matrix[x-i][y+i]==color){counter++;if(counter==5){return true;}}
             else {break;}
            }
         for(int i=1;i<5;++i){
             if(y-i<0||x+i>15){break;}
             else if(matrix[x+i][y-i]==color){counter++;if(counter==5){return true;}}
             else{break;}
         }
         //if no right bevel, reset counter and find left bevel.
         counter=1;
         for(int i=1;i<5;++i){
             if(y+i>15||x+i>15){break;}
             else if(matrix[x+i][y+i]==color){counter++;if(counter==5){return true;}}
             else {break;}
            }
         for(int i=1;i<5;++i){
             if(y-i<0||x-i<0){break;}
             else if(matrix[x-i][y-i]==color){counter++;if(counter==5){return true;}}
             else{break;}
         }

         // if all above no five same number return falie
         return false;


}





