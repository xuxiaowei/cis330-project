/*
* gomokuClient.cpp
* implementation file for longer functions in GomokuClient class
* author: Matthew Haynes
*/

#include "gomokuClient.hpp"

void GomokuClient::sendMatrix(int** board){
	//convert square array to linear array
	int* linBoard= new int[matrixSize];
	twoToOne(board, linBoard);
	//convert ints to network ints (int serialization)
	hostToNetArray(linBoard);
	int boardSize= sizeof(int) * matrixSize;
	int* boardPointer= &(linBoard[0]);
	//send matrix
	int bytesSent= send(ServerSock, (const void*)boardPointer, boardSize, 0);
	while(bytesSent < boardSize){
		boardPointer += bytesSent;
		boardSize -= bytesSent;
		bytesSent= send(ServerSock, (const void*)boardPointer, boardSize, 0);
	}
	delete[] linBoard;
}

void GomokuClient::receiveMatrix(int** board){
	int boardSize= sizeof(int) * matrixSize;
	int* linBoard= new int[matrixSize];
	int* boardPointer= &(linBoard[0]);
	//receive matrix
	int bytesRecv= recv(ServerSock, (void*)boardPointer, boardSize, 0);
	while(bytesRecv < boardSize){
		boardPointer += bytesRecv;
		boardSize -= bytesRecv;
		bytesRecv= recv(ServerSock, (void*)boardPointer, boardSize, 0);
	}
	//convert ints to host byte ordering (int 'de-serialization')
	netToHostArray(linBoard);
	//convert linear array to square array
	oneToTwo(board, linBoard);
	delete[] linBoard;
}

void GomokuClient::hostToNetArray(int* board){
	for(int i=0; i< matrixSize; i++){
		board[i]= htonl(board[i]);
	}
}

void GomokuClient::netToHostArray(int* board){
	for(int i=0; i< matrixSize; i++){
		board[i]=ntohl(board[i]);
	}
}

void GomokuClient::twoToOne(int** square, int* linear){
	for(int i=0; i< matrixWidth; i++){
		for(int j=0; j< matrixWidth; j++){
			linear[(matrixWidth * i) + j]= square[i][j];
		}
	}
}

void GomokuClient::oneToTwo(int** square, int* linear){
	for(int i=0; i< matrixWidth; i++){
		for(int j=0; j< matrixWidth; j++){
			square[i][j]= linear[(matrixWidth * i) + j];
		}
	}
}
