void CChess::AiGo(int& t, int &h, int[][] myBoard)
{
	int BoardData[2][15][15][8][2]={0};    // the matrix
	int k,i,j,q,b=0,a=1,d,y1=0,y2=0,x1=0,x2=0;
	int a1[15][15]={0},a2[15][15]={0};
	
	// fill in the matrix.
	for(k=0;k<2;k++)
		for(i=0;i<15;i++)
			for(j=0;j<15;j++)
			{
				if(myBoard[i][j]==0)
				{
					for(q=0;q<8;q++)
					{
						if(k==0) d=1;
						else d=2;
						if(q==0&&j>=0)
						{
							for(;j-a>=0;)
							{
								if(myBoard[i][j-a]==d){b++;a++;continue;}
								else break;
							}
							BoardData[k][i][j][q][0]=b;b=0;
							if(myBoard[i][j-a]==0&&j-a>=0){BoardData[k][i][j][q][1]=1;a=1;}
							else {BoardData[k][i][j][q][1]=0;a=1;}
						}
						if(q==1&&i>=0&&j>=0)
						{
							for(;i-a>=0&&j-a>=0;)
							{
								if(myBoard[i-a][j-a]==d){b++;a++;continue;}
								else break;
							}
							BoardData[k][i][j][q][0]=b;b=0;
							if(myBoard[i-a][j-a]==0&&j-a>=0&&i-a>=0){BoardData[k][i][j][q][1]=1;a=1;}
							else {BoardData[k][i][j][q][1]=0;a=1;}
						}
						if(q==2&&i>=0)
						{
							for(;i-a>=0;)
							{
								if(myBoard[i-a][j]==d){b++;a++;continue;}
								else break;
							}
							BoardData[k][i][j][q][0]=b;b=0;
							if(myBoard[i-a][j]==0&&i-a>=0){BoardData[k][i][j][q][1]=1;a=1;}
							else {BoardData[k][i][j][q][1]=0;a=1;}
						}
						if(q==3&&i>=0&&j<15)
						{
							for(;i-a>=0&&j+a<15;)
							{
								if(myBoard[i-a][j+a]==d){b++;a++;continue;}
								else break;
							}
							BoardData[k][i][j][q][0]=b;b=0;
							if(myBoard[i-a][j+a]==0&&i-a>=0&&j+a<15){BoardData[k][i][j][q][1]=1;a=1;}
							else {BoardData[k][i][j][q][1]=0;a=1;}
						}
						if(q==4&&j<15)
						{
							for(;j+a<15;)
							{
								if(myBoard[i][j+a]==d){b++;a++;continue;}
								else break;
							}
							BoardData[k][i][j][q][0]=b;b=0;
							if(myBoard[i][j+a]==0&&j+a<15){BoardData[k][i][j][q][1]=1;a=1;}
							else {BoardData[k][i][j][q][1]=0;a=1;}
						}
						if(q==5&&i<15&&j<15)
						{
							for(;i+a<15&&j+a<15;)
							{
								if(myBoard[i+a][j+a]==d){b++;a++;continue;}
								else break;
							}
							BoardData[k][i][j][q][0]=b;b=0;
							if(myBoard[i+a][j+a]==0&&i+a<15&&j+a<15){BoardData[k][i][j][q][1]=1;a=1;}
							else {BoardData[k][i][j][q][1]=0;a=1;}
						}
						if(q==6&&i<15)
						{
							for(;i+a<15;)
							{
								if(myBoard[i+a][j]==d){b++;a++;continue;}
								else break;
							}
							BoardData[k][i][j][q][0]=b;b=0;
							if(myBoard[i+a][j]==0&&i+a<15){BoardData[k][i][j][q][1]=1;a=1;}
							else {BoardData[k][i][j][q][1]=0;a=1;}
						}
						if(q==7&&j>=0&&i<15)
						{
							for(;i+a<15&&j-a>=0;)
							{
								if(myBoard[i+a][j-a]==d){b++;a++;continue;}
								else break;
							}
							BoardData[k][i][j][q][0]=b;b=0;
							if(myBoard[i+a][j-a]==0&&i+a<15&&j-a>=0){BoardData[k][i][j][q][1]=1;a=1;}
							else {BoardData[k][i][j][q][1]=0;a=1;}
						}
					}
				}
			}
			
			
			//evaluate every spot.
			for(k=0;k<2;k++)
				for(i=0;i<15;i++)
					for(j=0;j<15;j++)
					{
						if(k==0) // evaluate white.
						{
							for(q=0;q<4;q++)
							{
								if((BoardData[k][i][j][q][0]+BoardData[k][i][j][q+4][0])==4
									&&BoardData[k][i][j][q][1]==1&&BoardData[k][i][j][q+4][1]==1)
									b+=7000;
								if((BoardData[k][i][j][q][0]+BoardData[k][i][j][q+4][0])==3
									&&BoardData[k][i][j][q][1]==1&&BoardData[k][i][j][q+4][1]==1)
									b+=301;
								if((BoardData[k][i][j][q][0]+BoardData[k][i][j][q+4][0])==2
									&&BoardData[k][i][j][q][1]==1&&BoardData[k][i][j][q+4][1]==1)
									b+=43;
								if((BoardData[k][i][j][q][0]+BoardData[k][i][j][q+4][0])==1
									&&BoardData[k][i][j][q][1]==1&&BoardData[k][i][j][q+4][1]==1)
									b+=11;
								if((BoardData[k][i][j][q][0]+BoardData[k][i][j][q+4][0])==4
									&&((BoardData[k][i][j][q+4][1]==0)
									||(BoardData[k][i][j][q][1]==0)))
									b+=7000;
								if((BoardData[k][i][j][q][0]+BoardData[k][i][j][q+4][0])==3
									&&((BoardData[k][i][j][q][1]==1&&BoardData[k][i][j][q+4][1]==0)
									||(BoardData[k][i][j][q][1]==0&&BoardData[k][i][j][q+4][1]==1)))
									b+=63;
								if((BoardData[k][i][j][q][0]+BoardData[k][i][j][q+4][0])==2
									&&((BoardData[k][i][j][q][1]==1&&BoardData[k][i][j][q+4][1]==0)
									||(BoardData[k][i][j][q][1]==0&&BoardData[k][i][j][q+4][1]==1)))
									b+=6;
								if((BoardData[k][i][j][q][0]+BoardData[k][i][j][q+4][0])==1
									&&((BoardData[k][i][j][q][1]==1&&BoardData[k][i][j][q+4][1]==0)
									||(BoardData[k][i][j][q][1]==0&&BoardData[k][i][j][q+4][1]==1)))
									b+=1;
								
							}
							if(b==126||b==189||b==252) b=1500;
							if(b==106) b=1000;
							
							a1[i][j]=b;b=0;
						}
						if(k==1) //evaluate black
						{
							for(q=0;q<4;q++)
							{
								
								if((BoardData[k][i][j][q][0]+BoardData[k][i][j][q+4][0])==4
									&&BoardData[k][i][j][q][1]==1&&BoardData[k][i][j][q+4][1]==1)
									b+=30000;
								if((BoardData[k][i][j][q][0]+BoardData[k][i][j][q+4][0])==3
									&&BoardData[k][i][j][q][1]==1&&BoardData[k][i][j][q+4][1]==1)
									b+=1500;
								if((BoardData[k][i][j][q][0]+BoardData[k][i][j][q+4][0])==2
									&&BoardData[k][i][j][q][1]==1&&BoardData[k][i][j][q+4][1]==1)
									b+=51;
								if((BoardData[k][i][j][q][0]+BoardData[k][i][j][q+4][0])==1
									&&BoardData[k][i][j][q][1]==1&&BoardData[k][i][j][q+4][1]==1)
									b+=16;
								if((BoardData[k][i][j][q][0]+BoardData[k][i][j][q+4][0])==4
									&&((BoardData[k][i][j][q+4][1]==0)
									||(BoardData[k][i][j][q][1]==0)))
									b+=30000;
								if((BoardData[k][i][j][q][0]+BoardData[k][i][j][q+4][0])==3
									&&((BoardData[k][i][j][q][1]==1&&BoardData[k][i][j][q+4][1]==0)
									||(BoardData[k][i][j][q][1]==0&&BoardData[k][i][j][q+4][1]==1)))
									b+=71;
								if((BoardData[k][i][j][q][0]+BoardData[k][i][j][q+4][0])==2
									&&((BoardData[k][i][j][q][1]==1&&BoardData[k][i][j][q+4][1]==0)
									||(BoardData[k][i][j][q][1]==0&&BoardData[k][i][j][q+4][1]==1)))
									b+=7;
								if((BoardData[k][i][j][q][0]+BoardData[k][i][j][q+4][0])==1
									&&((BoardData[k][i][j][q][1]==1&&BoardData[k][i][j][q+4][1]==0)
									||(BoardData[k][i][j][q][1]==0&&BoardData[k][i][j][q+4][1]==1)))
									b+=2;
							}
							if(b==142||b==213||b==284) b=1500;
							if(b==122) b=1300;
							
							a2[i][j]=b;b=0;
						}
					}
					
					
					// pick the spot with highest score
					for(i=0;i<15;i++)
						for(j=0;j<15;j++)
						{
							if(a1[y1][x1]<a1[i][j]) {y1=i;x1=j;}
						}
						for(i=0;i<15;i++)
							for(j=0;j<15;j++)
							{
								if(a2[y2][x2]<a2[i][j]) {y2=i;x2=j;}
							}
							if(a2[y2][x2]>=a1[y1][x1]) {t=x2;h=y2;}
							else
							{t=x1;h=y1;}
}