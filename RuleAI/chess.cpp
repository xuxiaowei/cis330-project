
#include <iostream>
#include <cstring>

#include "chess.hpp"



CChess::CChess()
{
	memset(m_board,0,sizeof(m_board));
	m_turn=1;
	m_flag=0;
	posflag=0;
}

CChess::~CChess()
{
	
}

int CChess::Judge(int nx, int ny) // win or lose
{
	int count=0;
	for(int i=-4;i<5;i++)  // horizontal
	{
		if(m_board[ny][nx+i]==m_turn) {count++;if(count==5) return m_turn;}
		else count=0;
	}
	count=0;
	for(int i=-4;i<5;i++)  //vertical
	{
		if(m_board[ny+i][nx]==m_turn) {count++;if(count==5) return m_turn;}
		else count=0;
	}
	count=0;
	for(int i=-4;i<5;i++)  // slope
	{
		if(m_board[ny+i][nx+i]==m_turn) {count++;if(count==5) return m_turn;}
		else count=0;
		
	}
	count=0;
	for(int i=-4;i<5;i++)  //slope
	{
		if(m_board[ny+i][nx-i]==m_turn) {count++;if(count==5) return m_turn;}
		else count=0;
	}
	count=0;
	return 0;
}

void CChess::DrawQp() //initialize board
{

}

void CChess::DrawQz(int nx,int ny,int type)
{

}

bool CChess::DownZi(int nx, int ny,int type)
{

}

void CChess::ReDraw()
{
	DrawQp();
	for(int i=0;i<15;i++)
		for(int j=0;j<15;j++)
		{
			if(m_board[i][j]==1){DrawQz(j,i,1);}
			if(m_board[i][j]==2){DrawQz(j,i,2);}
		}
}

void CChess::NewGame(int type)
{
	memset(m_board,0,sizeof(m_board));
	m_flag=0;
	m_turn=1;
	posflag=0;
	if(type==0){m_board[7][7]=1;m_turn=2;
	posinfo[posflag].x=7;posinfo[posflag].y=7;posinfo[posflag].flag=2;
	posflag++;}
	ReDraw();
}



void CChess::AiGo(int& t, int &h)
{

}

void CChess::BackGo()
{
	m_board[posinfo[posflag-1].y][posinfo[posflag-1].x]=0;
	ReDraw();
}
