/*
* test-client.cpp
* test C-S-C communication with client() function
* author: Matthew Haynes
*/

#include "client.hpp"
#include <iostream>

int main(int argc, char* argv[]){
	if(argc != 2){
		std::cerr << "Wrong number of arguments entered." << std::endl;
		return 1;
	}	
	
	//make coordinates for sample piece
	int x= -1;
	int y= -1;

	std::cout << "*argv[1] - '0' is " << *argv[1] - '0' << " ." << std::endl;
	bool myTurn= (bool)(*argv[1] - '0');
	if(myTurn){
		std::cout << "It is my turn. Sending matrix." << std::endl;
		x= 4;
		y= 5;
		client(myTurn, x, y);
	} else {
		std::cout << "It is not my turn. Receiving matrix." << std::endl;
		client(myTurn, x, y);
		std::cout << "Coordinates of new Gomoku piece: " << '(' << x << ", " << y << ')' << std::endl;
	}
	return 0;
}
