/*
* gomokuServer.cpp
* implementation file for gomokuServer method functions
* author: Matthew Haynes
*/

#include "gomokuServer.hpp"

void GomokuServer::sendCoords(int sendSide, int receiveSide){
//	//tell sending client (sendSide) to send data now
//	int player1send= 0;//value does not matter, sending client will stall before sending board
//	send(sendSide, (const void*)&player1send, sizeof(int), 0);
	
	int x= -1;
	int y= -1;
	//receive coordinate information from sending client
	int bytesRecx= recv(sendSide, (void*)&x, sizeof(int), 0);
	int bytesRecy= recv(sendSide, (void*)&y, sizeof(int), 0);
//	while(bufSize - bytesRec > 0){//send rest of data
//		bufPointer += bytesRec;
//		bufSize -= bytesRec;
//		bytesRec= recv(sendSide, (void*)bufPointer, bufSize, 0);
//	}
	//send to receiving client
	int bytesSentx= send(receiveSide, (const void*)&x, sizeof(int), 0);
	int bytesSenty= send(receiveSide, (const void*)&y, sizeof(int), 0);
//	while(bufSize - bytesSent > 0){
//		bufPointer += bytesSent;
//		bufSize -= bytesSent;
//		bytesSent= send(receiveSide, (const void*)bufPointer, bufSize, 0);
//	}
}
