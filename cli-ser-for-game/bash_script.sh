#!/bin/bash
#author: Matthew Haynes
# compile client and server
make client
make server
#run server in background
./server &
#wait for socket binding
sleep 5
# first try running sending client, then receiving client
./client 1
./client 0
# second try running receiving client in background, then sending client
./client 0 &
./client 1
# clean up
make clean
