/*
* server.cpp
* (for a human-vs-human Gomoku interface) 
*/
extern "C"{
	#include <sys/types.h>
	#include <sys/socket.h>
	#include <netdb.h>
	#include <unistd.h>
	#include <netinet/in.h>
	#include <arpa/inet.h>
}

#include <cstring>//for memset()
#include <iostream>
#include "gomokuServer.hpp"//to create and manipulate GomokuServer object


//each call to this function is one Gomoku turn
void gomokuTurn(int SockDescr){
	//listen from socket, accept connection
	if(listen(SockDescr, 15)==-1){//listen at this socket
		std::cerr << "Error in listening at socket.\n";
		exit(1);
	}

	int ClientSockDescr0;//client file descriptor 0 to be communicating with
	int ClientSockDescr1;//client file descriptor 1 to be communicating with
	bool first= false;//is player 1 sending or receiving data?
	while(true){
		//get client file descriptor for player 1
		sockaddr Address0;//address of client
		socklen_t AddressSize0;//size of client address
		AddressSize0= sizeof(Address0);
		ClientSockDescr0= accept(SockDescr, &Address0, &AddressSize0);
		if(ClientSockDescr0!=-1){
			//is client sending or receiving data?
			int first1= 0;
			recv(ClientSockDescr0, (void*)&first1, sizeof(int), 0); 
			first= (bool)ntohl(first1);
			std::cout << "first is " << first << " ." << std::endl;
			break;
		}
	}
	//listen from socket, accept connection
	if(listen(SockDescr, 15)==-1){//listen at this socket
		std::cerr << "Error in listening at socket.\n";
		exit(1);
	}
	while(true){
		//now get the client file descriptor for player 2 (white)
		sockaddr Address1;//address of client
		socklen_t AddressSize1;//size of client address
		AddressSize1= sizeof(Address1);
		ClientSockDescr1= accept(SockDescr, &Address1, &AddressSize1);
		if(ClientSockDescr1!=-1){
			//no need for player 2's turn data (assume it is opposite of player 1's)
			int second1= 0;//unused variable to receive data from send
			recv(ClientSockDescr1, (void*)&second1, sizeof(int), 0);
			break;
		}
	}
	
	//transfer board matrix from one client to the other
	GomokuServer gomoku(ClientSockDescr0, ClientSockDescr1);
	if(first){//player 1 sending data
		gomoku.sendCoords(ClientSockDescr0, ClientSockDescr1);
	} else {//player 2 sending data
		gomoku.sendCoords(ClientSockDescr1, ClientSockDescr0);
	}
	//finally, close clients socket file descriptors
	if(close(ClientSockDescr0)==-1){
		std::cerr << "Error in closing socket with file descriptor " << ClientSockDescr0 << " .\n";
		exit(1);
	}
	if(close(ClientSockDescr1)==-1){
		std::cerr << "Error in closing socket with file descriptor " << ClientSockDescr1 << " .\n";
		exit(1);
	}
}

int main(int argc, const char* argv[])
{
	/*set constant port and address*/
	const char port[]= "8097";
	//const char host[]= "ix.cs.uoregon.edu";
	addrinfo AI_hints;
	memset(&AI_hints, 0, sizeof(AI_hints));
	//initialize necessary parameters of AI
	AI_hints.ai_flags= AI_PASSIVE;
	AI_hints.ai_family= AF_UNSPEC;
	AI_hints.ai_socktype= SOCK_STREAM; 
	
	addrinfo* AI;//will hold address information
	
	//get possible addresses with specified parameters
	if(getaddrinfo((const char*)0, port, &AI_hints, &AI)!=0){
		std::cerr << "Error in finding addresses.\n";
		return 1;
	}

	int SockDescr;//socket file descriptor to be initialized
	for(addrinfo* currAddr= AI; currAddr!= (addrinfo*)0 ;currAddr= AI->ai_next){
		SockDescr= socket(currAddr->ai_family, currAddr->ai_socktype, currAddr->ai_protocol);
		if(SockDescr!=-1){//break if no error occurred
			break;
		}
	}
	//bind to socket to listen from
	if(bind(SockDescr, AI->ai_addr, AI->ai_addrlen)==-1){
		std::cerr << "Error in binding to socket.\n";
		return 1;
	}
	freeaddrinfo(AI);//free address info before entering infinite loop!	
	
	//each iteration is one Gomoku game turn
	while(true){
		gomokuTurn(SockDescr);		
	}
	return 0;
}

