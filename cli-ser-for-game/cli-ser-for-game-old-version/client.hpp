/*
* header file for client.cpp
* author: Matthew Haynes
*/
#ifndef CLIENT_HPP
#define CLIENT_HPP

//to be called for every Gomoku turn
void client(bool myTurn, int** board);

#endif
