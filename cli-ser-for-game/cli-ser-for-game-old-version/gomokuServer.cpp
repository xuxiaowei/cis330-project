/*
* gomokuServer.cpp
* implementation file for gomokuServer method functions
* author: Matthew Haynes
*/

#include "gomokuServer.hpp"

void GomokuServer::sendMatrix(int sendSide, int receiveSide){
//	//tell sending client (sendSide) to send data now
//	int player1send= 0;//value does not matter, sending client will stall before sending board
//	send(sendSide, (const void*)&player1send, sizeof(int), 0);
	
	int buf[matrixSize];
	int bufSize= sizeof(int) * matrixSize;
	int* bufPointer= &(buf[0]);
	//receive board information from sending client
	int bytesRec= recv(sendSide, (void*)bufPointer, bufSize, 0);
	while(bufSize - bytesRec > 0){//send rest of data
		bufPointer += bytesRec;
		bufSize -= bytesRec;
		bytesRec= recv(sendSide, (void*)bufPointer, bufSize, 0);
	}
	//reset buf pointer
	bufPointer= &(buf[0]);
	bufSize= sizeof(int) * matrixSize;
	//send to receiving client
	int bytesSent= send(receiveSide, (const void*)bufPointer, bufSize, 0);
	while(bufSize - bytesSent > 0){
		bufPointer += bytesSent;
		bufSize -= bytesSent;
		bytesSent= send(receiveSide, (const void*)bufPointer, bufSize, 0);
	}
}
