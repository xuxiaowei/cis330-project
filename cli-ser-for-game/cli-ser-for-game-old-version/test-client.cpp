/*
* test-client.cpp
* test C-S-C communication with client() function
* author: Matthew Haynes
*/

#include "client.hpp"
#include <iostream>

int main(int argc, char* argv[]){
	if(argc != 2){
		std::cerr << "Wrong number of arguments entered." << std::endl;
		return 1;
	}	
	
	//heap-allocate square array
	int** board= new int*[15];
	for(int i=0; i<15; i++){
		board[i]= new int[15];
	}

	std::cout << "*argv[1] - '0' is " << *argv[1] - '0' << " ." << std::endl;
	bool myTurn= (bool)(*argv[1] - '0');
	if(myTurn){
		std::cout << "It is my turn. Sending matrix." << std::endl;
		for(int i=0; i<15; i++){
			for(int j=0; j<15; j++){
			board[i][j]= 15*i + j;
			}
		}
		client(myTurn, board);
	} else {
		std::cout << "It is not my turn. Receiving matrix." << std::endl;
		client(myTurn, board);
		int length= 0;
		std::cout << "2D board matrix: " << std::endl;
		for(int i=0; i<15; i++){
			for(int j=0; j<15; j++){
				std::cout << board[i][j] << ' ';
				length++;
			}
			std::cout << std::endl;
		}
		std::cout << std::endl;
		std::cout << "number of integers in 2D matrix: " << length << std::endl;
	}
	
	//delete heap-allocated square array
	for(int i=0; i<15; i++){
		delete[] board[i];
	}
	delete[] board;
	
	return 0;
}
