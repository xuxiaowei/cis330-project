/*
* gomokuClient.hpp
* author: Matthew Haynes
* class for human-vs-human gomoku communication with another client
*/

#ifndef GOMOKU_CLIENT_HPP
#define GOMOKU_CLIENT_HPP

extern "C"{
	#include <sys/socket.h>
	#include <sys/types.h>
	#include <unistd.h>
	#include <netinet/in.h>
	#include <arpa/inet.h>
}

#include <cstring>
#include <iostream>

class GomokuClient{
public:
	GomokuClient(int i_ServerSock) : ServerSock(i_ServerSock), matrixSize(225), matrixWidth(15) {}
	
	~GomokuClient() {}
		
	void sendMatrix(int** board);//send current board state to server
	
	void receiveMatrix(int** board);//receive current board state from server

private:
	void twoToOne(int** square, int* linear);//make linear array from 2-D array

	void oneToTwo(int** square, int* linear);//make 2-D array from linear array
	
	void hostToNetArray(int* board);//use htonl() on an array of ints, helper func to sendMatrix()

	void netToHostArray(int* board);//use ntohl() on an array of ints, helper func to receiveMatrix()
	
	int ServerSock;//socket file descriptor for server
	
	const int matrixSize;//size of gomoku board linear array

	const int matrixWidth;//width of 2-D gomoku board array
};
#endif
