/*
* gomokuServer.hpp
* author: Matthew Haynes
* helper class for server.cpp to use to communicate between two clients
*/

#ifndef GOMOKU_SERVER_HPP
#define GOMOKU_SERVER_HPP
extern "C"{
	#include <unistd.h>
	#include <sys/socket.h>
	#include <sys/types.h>
	#include <netinet/in.h>
	#include <arpa/inet.h>
}
#include <cstring>
#include <iostream>

class GomokuServer {
public:
	GomokuServer(int i_sockdescr0, int i_sockdescr1) : SockDescr0(i_sockdescr0), 
		SockDescr1(i_sockdescr1) {}
	//initialize socket descriptors for the two clients, initialize constant matrix size
	
	~GomokuServer() {}
	
	/*send coords representing new piece from a
	given socket descriptor representing a player 'sendSide', to the other client player 'receiveSide'
	*/
	void sendCoords(int sendSide, int receiveSide);
	//passing in member variables for good code reuse; otherwise two functions necessary for sending either way
private:
	int SockDescr0; //socket descriptor for black player (player 1)
	
	int SockDescr1; //socket descriptor for white player (player 2)
};
#endif
